<?php
/**
 * Funtion sendMail
 */
if (!isset($_POST) || !isset($_POST['email'])) {
    $msg = 'No se han recibido datos.';
    echo '<div class="alert alert-danger"><p><i class="fa fa-exclamation-triangle"></i> ' . $msg . '</p></div>';
    return false;
}

if ($_POST['email'] == '') {
    //ERROR: FIELD "email" EMPTY
    $msg = 'Por favor introduzca una dirección de correo electrónico válida.';
    echo '<div class="alert alert-danger"><p><i class="fa fa-exclamation-triangle"></i> ' . $msg . '</p></div>';
    return false;
}

//send mail
mail("info@mgcampamento.es", " Notifícame!", "Email: " . $_POST['email']);

//success message:
$msg = 'Su Email se ha guardado con éxito.';

echo '<div class="alert alert-success"><p><i class="fa fa-check"></i> ' . $msg . '</p></div>';
return true;

?>
